package com.company;

import java.util.Arrays;

public class Wrapper <E>{
    private static int defaultValue=0,count=0;
    private Object[] obj=new Object[defaultValue];

    private int size;
    int i=0;

    public void addItem(E e) throws NumberFormatException,DubplicateException{

        if(e==null){
            throw new NumberFormatException("input null value");
        }

        for( Object i:obj){
            if(i==e){
                throw new DubplicateException("Value = "+e+" is all ready exist !!");
            }
        }

        if (defaultValue == count) {
            defaultValue++;

            obj = Arrays.copyOf(obj, defaultValue);
            obj[count] = e;
            count++;
        }
    }

    public Object getItem(int index){
        return  obj[index];
    }

    public int size(){
        return obj.length;
    }
}
