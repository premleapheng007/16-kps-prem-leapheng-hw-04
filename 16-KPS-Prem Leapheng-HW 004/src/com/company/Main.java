package com.company;

public class Main {

    public static void main(String[] args) {

        Wrapper<Integer> obj= new Wrapper<>();

//        try {
//            obj.addItem(3);
//            obj.addItem(5);
//            obj.addItem(null);
//            obj.addItem(3);
//
//        }catch (Exception e){
//            System.out.println(e);
//        }
        try {
            for(int i=0;i<100;i++){
                obj.addItem(i);
            }
        }catch (Exception e){
            System.out.println(e);
        }


        System.out.println("=======================");
        System.out.println("  List all from input  ");
        System.out.println("=======================\n");

        for(int i=0; i<obj.size();i++){
            System.out.println((i+1)+". "+obj.getItem(i));
        }

    }
}
